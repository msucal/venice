## Venice

This is a theme framework for WordPress. 
It is a child theme of OceanWP, and is built around the use of Elementor templates.

Adding this theme to a clean WordPress instance will start the installation process.
Venice uses TGM to install GitHub Updater in order to keep itself up-to-date.

This theme integrates an API key for the MSU GitLab instance and should not be distributed outside of the University.

## Requirements

Venice is a child theme of [OceanWP](https://wordpress.org/themes/oceanwp).

This theme requires the following plugins:
* [Ocean Extra](https://wordpress.org/plugins/ocean-extra) adds sub-templating support for OceanWP and its child themes.
* [Elementor](https://wordpress.org/plugins/elementor) is a visual page builder. Venice's templates require Elementor to function.
* [Press Elements](https://wordpress.org/plugins/press-elements) adds additional blocks to Elementor that are variable based on page content.
* [GitHub Updater](https://github.com/afragen/github-updater) provides auto-update support for themes and plugins hosted in git repos.