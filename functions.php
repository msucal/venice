<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: oceanwp
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
function venice_enqueue_parent_style() {
	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'OceanWP' );
	$version = $theme->get( 'Version' );
	// Load the stylesheet
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'oceanwp-style' ), $version );
	
}
add_action( 'wp_enqueue_scripts', 'venice_enqueue_parent_style' );

function venice_customizer_defaults( $wp_customize ) {
	set_theme_mod( 'ocean_hover_primary_color', '#0db14b' );
	set_theme_mod( 'ocean_theme_button_hover_bg', '#0db14b' );
	set_theme_mod( 'ocean_primary_color', '#18453b' );
	set_theme_mod( 'ocean_links_color_hover', '#0db14b' );
	set_theme_mod( 'ocean_breadcrumbs_link_color_hover', '#0db14b' );
	set_theme_mod( 'ocean_theme_button_bg', '#18453b' );
	set_theme_mod( 'ocean_top_bar_content', esc_html__( ' ', 'oceanwp' ) );
	set_theme_mod( 'ocean_top_bar_bg', '#18453b' );
	set_theme_mod( 'ocean_top_bar_border_color', '#18453b' );
}
add_action( 'customize_register', 'venice_customizer_defaults' );

function venice_tgmpa_register() {
	// Get array of recommended plugins
	$plugins = array(
		
		array(
			'name'				=> 'Ocean Extra',
			'slug'				=> 'ocean-extra', 
			'required'			=> true,
			'force_activation'	=> false,
		),
		
		array(
			'name'				=> 'Elementor',
			'slug'				=> 'elementor', 
			'required'			=> true,
			'force_activation'	=> false,
		),

		array(
			'name'				=> 'Press Elements',
			'slug'				=> 'press-elements', 
			'required'			=> true,
			'force_activation'	=> false,
		),

		array(
			'name'				=> 'GitHub Updater',
			'slug'				=> 'github-updater',
			'source'			=> 'https://github.com/afragen/github-updater/archive/7.5.0.zip', 
			'required'			=> true,
			'force_activation'	=> false,
			'external_url'		=> 'https://github.com/afragen/github-updater',
		),

		
		
	);
	// Register notice
	tgmpa( $plugins, array(
		'id'           => 'venice_theme',
		'domain'       => 'venice',
		'menu'         => 'install-required-plugins',
		'has_notices'  => true,
		'is_automatic' => true,
		'dismissable'  => true,
	) );
}
add_action( 'tgmpa_register', 'venice_tgmpa_register' );